// See COPYING for licence details.

#import "NSObject+ObserverPattern.h"
#import <objc/runtime.h>
#import <objc/message.h>

NSString *getter_from_setter(NSString *setter)
{
    //remove the final character
    NSString *getter = [setter substringToIndex:setter.length - 1];
    //remove the "set"
    getter = [getter substringFromIndex:3];
    //lower-case the first letter
    getter = [getter stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[getter substringToIndex:1] lowercaseString]];
    return getter;
}

NSString *setter_from_key(NSString *key)
{
    //capitalise the first letter
    NSString *setter = [key stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[key substringToIndex:1] uppercaseString]];
    //add 'set' and the colon
    setter = [NSString stringWithFormat:@"set%@:", setter];
    return setter;
}

static NSString *GJLObserverPatternClassPrefix = @"GJLObserverPattern";
static NSString *GJLObserverPatternAssociatedArrayKey = @"GJLObserverPattern";
static NSString *GJLObserverClassOriginalClassKey = @"GJLObserverClassOriginalClassKey";

@interface GJLObservationInformation : NSObject
{
    @package
    NSString *key;
    GJLObserverBlock block;
}
@end
@implementation GJLObservationInformation

- (void)dealloc
{
    [key release];
    Block_release(block);
    [super dealloc];
}

@end

void replaced_setter(id self, SEL _cmd, id newValue)
{
    NSString *setterName = NSStringFromSelector(_cmd);
    NSString *getterName = getter_from_setter(setterName);
    id oldValue = [self valueForKey:getterName];
    
    //the next few lines implement [super set<Foo>:]
    struct objc_super superclass = { .receiver = self, .super_class = class_getSuperclass(object_getClass(self))};
    objc_msgSendSuper(&superclass, _cmd, newValue);
    // look up observers
    NSMutableArray *observers = objc_getAssociatedObject(self, GJLObserverPatternAssociatedArrayKey);
    // call their blocks
    for (GJLObservationInformation *info in observers)
    {
        if ([info->key isEqualToString:getterName])
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                info->block(self, getterName, oldValue, newValue);
            });
        }
    }
}

void replaced_dealloc(id self, SEL _cmd)
{
    //delete the associated objects
    objc_setAssociatedObject(self, GJLObserverPatternAssociatedArrayKey, nil, OBJC_ASSOCIATION_ASSIGN);
    //super dealloc
    struct objc_super superclass = { .receiver = self, .super_class = class_getSuperclass(object_getClass(self))};
    objc_msgSendSuper(&superclass, _cmd);
}

Class replaced_class(id self, SEL _cmd)
{
    Class OriginalClass = objc_getAssociatedObject(object_getClass(self), GJLObserverClassOriginalClassKey);
    return OriginalClass;
}

@implementation NSObject (GJLObserverPattern)

- (id)addObserverForKey:(NSString *)key withBlock:(GJLObserverBlock)block
{
    //check that I have a setter for that key (on this class or any superclass)
    NSString *setterName = setter_from_key(key);
    SEL setterSelector = NSSelectorFromString(setterName);
    Method setterMethod = class_getInstanceMethod([self class], setterSelector);
    if (!setterMethod)
    {
        @throw [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"Object %@ does not have a conventional setter for key %@", self, key] userInfo:nil];
        return nil;
    }
    //is this already an observer class?
    Class ObserverClass = object_getClass(self);
    Class OriginalClass = ObserverClass;
    NSString *className = NSStringFromClass(OriginalClass);
    if ([className hasPrefix:GJLObserverPatternClassPrefix])
    {
        //not an observer class: does the class already exist?
        NSString *prefixedName = [GJLObserverPatternClassPrefix stringByAppendingString:className];
        ObserverClass = NSClassFromString(prefixedName);
        if (ObserverClass == Nil)
        {
            //class doesn't exist: create it
            ObserverClass = objc_allocateClassPair(OriginalClass, [prefixedName UTF8String], 0);
            class_addMethod(ObserverClass, @selector(dealloc), (IMP)replaced_dealloc, "v@:");
            objc_setAssociatedObject(ObserverClass, GJLObserverClassOriginalClassKey, OriginalClass, OBJC_ASSOCIATION_ASSIGN);
            class_addMethod(ObserverClass, @selector(class), (IMP)replaced_class, "@@:");
            objc_registerClassPair(ObserverClass);
        }
        //change the class of this object
        object_setClass(self, ObserverClass);
    }
    //does this class (not superclasses) already implement the setter?
    unsigned int methodCount = 0;
    Method *methodList = class_copyMethodList(ObserverClass, &methodCount);
    BOOL hasMethod = NO;
    for (unsigned int i = 0; i < methodCount; i++)
    {
        SEL thisSelector = method_getName(methodList[i]);
        if (thisSelector == setterSelector)
        {
            hasMethod = YES;
            break;
        }
    }
    free(methodList);
    
    if (hasMethod == NO)
    {
        //it does not: add our setter function as the setter for this key
        class_addMethod(ObserverClass, setterSelector, (IMP)replaced_setter, "v@:@");
    }
    //generate a token object for this observation
    GJLObservationInformation *token = [GJLObservationInformation new];
    token->key = [key copy];
    token->block = Block_copy(block);
    //record the observation info for this observation
    NSMutableArray *observers = objc_getAssociatedObject(self, GJLObserverPatternAssociatedArrayKey);
    if (observers == nil)
    {
        observers = [NSMutableArray array];
        objc_setAssociatedObject(self, GJLObserverPatternAssociatedArrayKey, observers, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    [observers addObject:token];
    [token release];
    //return the token
    return token;
}

- (void)removeObserverWithToken:(id)token
{
    //look up the observation info for this token
    NSMutableArray *observers = objc_getAssociatedObject(self, GJLObserverPatternAssociatedArrayKey);
    for (GJLObservationInformation *info in [[observers copy] autorelease])
    {
        if ([info isEqual:token])
        {
            //delete the observation info reference
            [observers removeObject:info];
            break;
        }
    }
}
@end
