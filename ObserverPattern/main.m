// See COPYING for licence details.

#import <Foundation/Foundation.h>
#import "NSObject+ObserverPattern.h"

@interface GJLFriendlyMessage : NSObject

@property (nonatomic, copy) NSString *text;

@end
@implementation GJLFriendlyMessage
@end

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        GJLFriendlyMessage *message = [[GJLFriendlyMessage alloc] init];
        id token = [message addObserverForKey:@"text" withBlock:^(id observableObject, NSString *key, id oldValue, id newValue){
            NSLog(@"%@.%@ is now %@", observableObject, key, newValue);
        }];
        message.text = @"Hello London";
        message.text = @"tech talk";
        [message removeObserverWithToken:token];
        sleep(1);
    }
    return 0;
}

